import { mutationField, idArg, stringArg } from "nexus/dist";
import { checkAuthentication } from "../utility/authentication";

export const CreateCube = mutationField("createCube", {
    type: 'Cube',
    args: {
      name: stringArg(),
      description: stringArg({ nullable: true})
    },
    resolve: (_, { name, description }, ctx) => {
      checkAuthentication(ctx);
      return ctx.prisma.createCube({
        name,
        description,
        owner: { connect: { id: ctx.user.id }}
      })
    }
  });
    
export default CreateCube;