import { mutationField, stringArg } from "nexus/dist";
import { compare } from "bcryptjs";
import { cryptoSecret } from "../utility/constants";
import { sign, verify } from 'jsonwebtoken'


export const Login = mutationField("login", {
    type: 'LoginResponse',
    args: {
      email: stringArg(),
      password: stringArg()
    },
    resolve: async (_, { email, password}, ctx) => {
      const user = await ctx.prisma.user({ email })
      if (!user) {
        throw new Error('Invalid login')
      }
      const passwordMatch = await compare(password, user.password)
      if (!passwordMatch) {
        throw new Error('Invalid Login')
      }
      const token = sign(
        {
          id: user.id,
          username: user.email,
        },
        cryptoSecret,
        {
          expiresIn: '30d', // token will expire in 30days
        },
      )
      return {
        token,
        user,
      }
    }
  });

export default Login;