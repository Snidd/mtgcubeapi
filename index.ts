import { prisma } from './generated/prisma-client'
import datamodelInfo from './generated/nexus-prisma'
import * as path from 'path'
import { prismaObjectType, makePrismaSchema } from 'nexus-prisma'
import { GraphQLServer } from 'graphql-yoga'
import { getUser } from './utility/authentication'
import * as allTypes from './types'

const Query = prismaObjectType({
  name: 'Query',
  definition(t) {
    t.prismaFields(['cube'])
    t.prismaFields(['cubeChanges'])
    t.field('currentUser', {
      type: 'User',
      resolve: (_, args, { user, prisma }) => {
        if (!user) {
          throw new Error('Not Authenticated')
        }
        return prisma.user({ id: user.id })
      }
    })
    t.list.field('cubes', {
      type: 'Cube',
      resolve: (_, args, ctx) =>
        ctx.prisma.cubes()
    })
  },
})

//cjxzw592400410808g8mrw19e
const Mutation = prismaObjectType({
  name: 'Mutation',
  definition(t) {  
  },
})

const schema = makePrismaSchema({
  types: [Query, Mutation, allTypes],

  prisma: {
    datamodelInfo,
    client: prisma,
  },

  outputs: {
    schema: path.join(__dirname, './generated/schema.graphql'),
    typegen: path.join(__dirname, './generated/nexus.ts'),
  },
})

const server = new GraphQLServer({
  schema,
  context: (req) => {
    const tokenWithBearer = req.request.headers.authorization || ''
    const token = tokenWithBearer.split(' ')[1]
    const user = getUser(token)

    return {
      user,
      prisma, // the generated prisma client if you are using it
    } 
  },
})
server.start(() => console.log('Server is running on http://localhost:4000'))
