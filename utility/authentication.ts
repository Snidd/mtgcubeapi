import { mutationField, stringArg } from "nexus/dist";
import { compare, hash } from "bcryptjs";
import { sign, verify } from 'jsonwebtoken'
import { cryptoSecret } from './constants'

export const getUser = (token) => {
  try {
    if (token) {
      return verify(token, cryptoSecret)
    }
    return null
  } catch (err) {
    return null
  }
}

export const checkAuthentication = (ctx: any) => {
  if (!ctx.user) {
    throw new Error('Not authenticated');
  }
}