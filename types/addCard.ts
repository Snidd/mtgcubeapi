import { mutationField, idArg, stringArg } from "nexus/dist";
import { checkAuthentication } from "../utility/authentication";

export const AddCard = mutationField("addCard", {
    type: 'Card',
    args: {
      cubeId: idArg({ nullable: false}),
      name: stringArg(),
      pictureUrl: stringArg({nullable: true})
    },
    resolve: async (_, { cubeId, name, pictureUrl}, ctx) => {
      const owner = await ctx.prisma.cube({ id: cubeId}).owner()
      checkAuthentication(ctx);
      if (owner.id !== ctx.user.id) {
        throw new Error("Not your cube!")
      }
      const card = await ctx.prisma.createCard({
        name,
        pictureUrl,
        cube: { connect: { id: cubeId }}
      })
      await ctx.prisma.createCubeChanges({
        card: { connect: { id: card.id }},
        amount: 1,
        cube: { connect: { id: cubeId }}
      })
      return card;
    }
  });

export default AddCard;