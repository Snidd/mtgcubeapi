import { objectType } from "nexus/dist";

// Custom objectType
export const LoginResponse = objectType({
    name: "LoginResponse",
    definition: (t) => {
      t.string("token", { description: "The token to use for authentication if successfully logged in" });
      t.field("user", {
        type: 'User'
      })
    },
  });

export default LoginResponse