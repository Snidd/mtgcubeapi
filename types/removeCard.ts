import { mutationField, idArg, stringArg } from "nexus/dist";
import { checkAuthentication } from "../utility/authentication";

export const RemoveCard = mutationField("removeCard", {
    type: 'Card',
    args: {
      cubeId: idArg({ nullable: false}),
      name: stringArg()
    },
    resolve: async (_, { cubeId, name, pictureUrl}, ctx) => {
      const owner = await ctx.prisma.cube({ id: cubeId}).owner()
      checkAuthentication(ctx);
      if (owner.id !== ctx.user.id) {
        throw new Error("Not your cube!")
      }
      // TODO: Complete this
    }
  });

export default RemoveCard;