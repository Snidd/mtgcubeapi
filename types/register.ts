import { hash } from "bcryptjs";
import { stringArg, mutationField } from "nexus/dist";

export const Register = mutationField("register", {
    type: 'User',
    args: {
      email: stringArg(),
      name: stringArg(),
      password: stringArg()
    },
    resolve: async (_, { email, name, password}, ctx) => {
      const hashedPassword = await hash(password, 10)
      const user = await ctx.prisma.createUser({
        email,
        name,
        password: hashedPassword,
      })
      return user
    }
  });

export default Register;